# Conky configuration file

To install in Ubuntu

```bash
sudo apt install conky-all
sudo apt install lm-sensors hddtemp
mkdir .config/conky
cp /etc/conky/conky.conf ~/.config/conky/
printf '#!/bin/bash\nconky -c ~/.config/conky/conky.conf &\n' >> ~/.local/bin/conky_start
chmod +x ~/.local/bin/conky_start
~/.local/bin/conky_start 
```